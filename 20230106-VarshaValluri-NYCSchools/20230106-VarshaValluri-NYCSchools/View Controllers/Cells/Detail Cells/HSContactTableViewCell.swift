//
//  HSContactTableViewCell.swift
//  20180817-VarshaValluri-NYCSchools
//
//  Created by Varsha on 8/18/18.
//  Copyright © 2018 Varsha. All rights reserved.
//

import UIKit

class HSContactTableViewCell: UITableViewCell {

    @IBOutlet var hsAddressLbl: UILabel!
    @IBOutlet var hsPhoneLbl: UILabel!
    @IBOutlet var hsWebsiteLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
