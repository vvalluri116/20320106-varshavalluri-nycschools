//
//  HSSatScoresTableViewCell.swift
//  20180817-VarshaValluri-NYCSchools
//
//  Created by Varsha on 8/18/18.
//  Copyright © 2018 Varsha. All rights reserved.
//

import UIKit

class HSSatScoresTableViewCell: UITableViewCell {

    @IBOutlet var hsNameLbl: UILabel!
    @IBOutlet var satReadingAvgScoreLbl: UILabel!
    @IBOutlet var satMathAvgScoreLbl: UILabel!
    @IBOutlet var satWritingAvgScoreLbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
