//
//  NYCHighSchools.swift
//  20180817-VarshaValluri-NYCSchools
//
//  Created by Varsha on 8/17/18.
//  Copyright © 2018 Varsha. All rights reserved.
//

import UIKit

class NYCHighSchools: NSObject {

    var dbn: String?
    var schoolName: String?
    var overviewParagraph: String?
    var schoolAddress: String?
    var schoolWebsite:String?
    var schoolTelephoneNumber: String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore: String?
    var satWritinAvgScore: String?

}
